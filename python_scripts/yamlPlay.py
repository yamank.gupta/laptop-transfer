import sys
import yaml
import subprocess

with open(str(sys.argv[1])) as file:
    input_file = yaml.load(file)

for data in input_file:
    for my_ip in data:
        for port_data in data[my_ip]:
            for port in port_data:
                for d in port_data[port]:
                    command = f'sudo iptables -A INPUT -s {d} -d {my_ip} -p tcp --destination-port {port} -j ACCEPT'
                    print(command)
                    result = subprocess.run(command,
                                            shell=True,
                                            capture_output=True)


result_2 = subprocess.run('sudo iptables -L ',
                          shell=True,
                          capture_output=True)

print(result_2.stdout.decode())
