#!/bin/bash


os=$(grep "^NAME" /etc/os-release | cut -d'"' -f 2)

case $os in
"Ubuntu")
    ./ubuntu.sh
    ;;

"CentOS")
    ./centos.sh centos
    ;;
"Redhat Enterprise Linux (RHEL)")
    ./centos rhel
    ;;

"Mac OSX")
    ./mac.sh
    ;;
"Windows")
    echo -e "\nCan be installed via  docker only "
    ;;
*)
    echo -e "\nSorry this OS isnt supported by this Utility"
    ;;
esac
