#!/bin/bash

echo -e "\nWhich Version you want to install ? \nFollowing is the list possible versions available : \n"
apt-cache madison getenvoy-envoy | awk -F'|' '{print $2}' | cat -n >versions
cat versions
echo -e "\nPlease provide the number from the available releases.\n (by default latest will be installed) "
read input_version

version=$(awk -v ver="$input_version" 'NR==ver {print $2} ' versions)

sudo apt update
sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -sL 'https://getenvoy.io/gpg' | sudo apt-key add -
# verify the key
apt-key fingerprint 6FF974DB | grep "5270 CEAC"
sudo add-apt-repository "deb [arch=amd64] https://dl.bintray.com/tetrate/getenvoy-deb $(lsb_release -cs) stable"
sudo apt update
if [[ -z $version ]]; then
    sudo apt install getenvoy-envoy
else
    sudo apt install --allow-downgrades getenvoy-envoy=$version -V
fi

if [[ $? == 0 ]]; then
    echo -e "\nCongratualtions , Envoy Proxy has been succesfully installed in your system.\nThis is the installed version "
    envoy --version
    else
    echo -e "Sorry User , installation failed at some point , Please try again. "
fi
