#!/bin/bash

echo -e "\nWhich Version you want to install ? \n Following is the list possible versions available : \n"
yum --showduplicates list getenvoy-envoy | expand | tail -n +7 | awk '{print $2}' | cat -n >versions
cat versions
echo -e "\nPlease provide the number from the available releases.\n (by default latest will be installed) "
read input_version

version=$(awk -v ver="$input_version" 'NR==ver {print $2} ' versions)

sudo yum install yum-utils
sudo yum-config-manager --add-repo https://getenvoy.io/linux/$1/tetrate-getenvoy.repo
if [[ -z $version ]]; then
    sudo yum install getenvoy-envoy
else
    sudo yum install getenvoy-envoy-$version
fi
if [[ $? == 0 ]]; then
    echo -e "\nCongratualtions , Envoy Proxy has been succesfully installed in your system.\nThis is the installed version "
    envoy --version
    else
    echo -e "Sorry User , installation failed at some point , Please try again. "
fi