#!/bin/bash

echo -e "\nWhich Version you want to install ? \n Following is the list possible versions available : \n"
brew search envoy
echo -e "\nPlease provide the number from the available releases.\n (by default latest will be installed) "
read input_version
brew update
brew install envoy $input_version
if [[ $? == 0 ]]; then
    echo -e "\nCongratualtions , Envoy Proxy has been succesfully installed in your system.\nThis is the installed version "
    envoy --version
else
    echo -e "Sorry User , installation failed at some point , Please try again. "
fi
