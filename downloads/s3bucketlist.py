import boto3
import pprint

'''#create the object to run the specific service
# ec2_ob = boto3.resource('ec2')
# iam_ob = boto3.resource('iam')
ec2_ob = boto3.resource('ec2')
#list the aws instances 
for instance in ec2_ob.instances.all():
    print(instance)'''

# --------creating session and doing this--------
# resuource id
session = boto3.Session(profile_name="default")
ec2_cli = session.client(service_name='ec2')


def ec2_resource():
    ec2_re = session.resource(service_name='ec2')
    for instance in ec2_re.instances.all():
        print(instance.id, instance.state["Name"])


if __name__ == "__main__":
    pprint.pprint(ec2_cli.describe_instances()['Reservations'])
    ec2_resource()
